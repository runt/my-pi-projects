#! /usr/bin/env python

import fibo

for i in range(1,100,2):
    print '{0:2d} {1:10d}'.format(i,i*i)

print "-" * 80

print fibo.fib(2000)

seznam = [66.25,333,333,1,1234.5]
print "-" * 80
print seznam
print "-" * 80
seznam.insert(2,-1);seznam.append(10)
print "-" * 80
print seznam
seznam.extend(seznam)
print "-" * 80
print seznam
seznam.reverse()
print "-" * 80
print seznam
seznam.sort()
print "-" * 80
print seznam

def cube(x):
    return x*x*x

print "-" * 80
print map(cube,range(1,5))

#tuples
t=651651,654654,65406,5406,5465,406,540,'fsfs','dsfsfsdfsfs'
print t

u = t,9,8,7,6,4,3
print u[0]
print u[1]

print dir(fibo)
